import random

def pi_estimation(nbr_estimates):
    """Monte Carlo estimate of the number of points in a
       quarter circle using pure Python"""

    print("Executing estimate_nbr_points_in_quarter_circle" \
            f"with {nbr_estimates}")

    nbr_trials_in_quarter_unit_circle = 0

    for _ in range(int(nbr_estimates)):
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)
        is_in_unit_circle = x * x + y * y <= 1.0
        nbr_trials_in_quarter_unit_circle += is_in_unit_circle

    return nbr_trials_in_quarter_unit_circle * 4 / float(nbr_estimates)

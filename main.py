import time

from montecarlo import pi_estimation

def main():

    start_time = time.time()
    nbr_samples_in_total = 1e8
    pi = pi_estimation(nbr_samples_in_total)

    print(f'Estimated pi', pi)
    elapsed = time.time() - start_time
    print(f'Done in {elapsed} seconds')


if __name__ == '__main__':
    main()
